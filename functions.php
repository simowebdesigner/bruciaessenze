<?php
function makali_child_enqueue_styles()
{

    $parent_style = 'makali-style'; // This is 'makali-style' for the Makali theme.

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style(
        'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
    );
}
add_action('wp_enqueue_scripts', 'makali_child_enqueue_styles');
/*custom plugin wait list*/
add_filter('wcwl_join_waitlist_button_text', 'change_waitlist_join_button_text');
function change_waitlist_join_button_text($text)
{
    return __('Iscriviti alla lista di attesa');
}

add_action('wp_head', 'my_analytics', 20);
function my_analytics()
{
?>

   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-3WE6JZGCMV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-3WE6JZGCMV');
</script>

<?php
}


function show_stock()
{
    global $product;
    if ($product->get_stock_quantity()) { // if manage stock is enabled 
        if (number_format($product->get_stock_quantity(), 0, '', '') == 1) { // if stock is low
            echo '<div class="remaining fs" style="color:#13ba3a;">' . number_format($product->get_stock_quantity(), 0, '', '') . ' disponibile</div>';
        } else if (number_format($product->get_stock_quantity(), 0, '', '') < 3) { // if stock is low
            echo '<div class="remaining" style="color:#13ba3a;">' . number_format($product->get_stock_quantity(), 0, '', '') . ' disponibili</div>';
        } else {
            echo '<div class="remaining" style="color:#13ba3a;">' . number_format($product->get_stock_quantity(), 0, '', '') . ' disponibili</div>';
        }
    } else {

        echo '<div class="remaining" style="color:#ac0146;"><a href="' . get_the_permalink() . '">Iscriviti alla lista di attesa</a></div>';
    }
}

add_action('woocommerce_after_shop_loop_item', 'show_stock', 10);

//css theme
function example_enqueue_styles()
{

    // enqueue parent styles
    wp_enqueue_style('parent-theme', get_stylesheet_directory_uri() . '/custom.css');
}
add_action('wp_enqueue_scripts', 'example_enqueue_styles', 28);

//shortcode taxonomies woocommecerce
function myshort($attr, $content = null) {
    ob_start(); ?>
    <?php
    // Define attributes and their defaults
    // Defining Shortcode's Attributes
    $shortcode_args = shortcode_atts(
        array(
            'include'     => '',
            'columns' => '',
            'style' => ''

        ), $attr);
    // Define query parameters based on attributes
    $terms = get_terms( array(
        'taxonomy' => 'product_cat',
        'hide_empty' => true,
        'include' => $shortcode_args['include'],
        'orderby'  => 'include', // <--- 
    ) );
    $col = ($shortcode_args['columns']) ? $shortcode_args['columns'] : 'col-sm-6 col-md-4';
    $style_row = ($shortcode_args['style'] == 'style2' || $shortcode_args['style'] == 'style3') ? 'row' : '';
    $style_col = ($shortcode_args['style'] == 'style2' || $shortcode_args['style'] == 'style3') ? 'col-sm-6' : '';
    $height_img = ($shortcode_args['style'] == 'style2' || $shortcode_args['style'] == 'style3') ? 'style="height:auto;"' : '';
    ?>

        <?php if ($terms) { ?>
            <div class="row tax-woocommerce-shortcode <?php echo $shortcode_args['style']; ?>">
            <?php foreach ($terms as $term) : ?>
                <?php
                $term_id = $term->term_id;
                //$image_url = get_wp_term_image( $term_id );
                $thumbnail_id = get_term_meta($term_id, 'thumbnail_id', true);
                $image_url = wp_get_attachment_image_url($thumbnail_id, 'large');
                ?>
                <div class="<?php echo $col; ?> item-column">
                    <div class="box-item <?php echo $style_row; ?>">
                        <?php if ($image_url) { ?>
                            <div class="box-img <?php echo $style_col; ?>">
                                <a href="<?php echo get_term_link($term); ?>" title=" <?php echo $term->name; ?>">
                                    <img src="<?php echo esc_url($image_url); ?>" alt="<?php echo $term->name; ?>" <?php echo $height_img; ?> />
                                </a>
                            </div>
                        <?php } ?>
                        <div class="box-text <?php echo $style_col; ?>">
                            <a href="<?php echo get_term_link($term); ?>" title=" <?php echo $term->name; ?>">
                                <h3 class="item-title"><?php echo $term->name; ?></h3>
                            </a>
                            <div class="box-term-description"><?php echo $term->description; ?></div>
                            <a class="item-button" href="<?php echo get_term_link($term); ?>" title=" <?php echo $term->name; ?>"><?php _e('Vedi i prodotti','bruciaessenze') ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        <?php }

    return ob_get_clean();
}

add_shortcode('woo_tax', 'myshort');

//order woocommerce
add_filter( 'woocommerce_get_catalog_ordering_args', 'bbloomer_first_sort_by_stock_amount', 9999 );
 
function bbloomer_first_sort_by_stock_amount( $args ) {
   $args['orderby'] = 'menu_order';
   $args['order'] = 'ASC';
   //$args['meta_key'] = '_stock_status';
   return $args;
}
//term description
//add_action( 'woocommerce_before_shop_loop', 'woocommerce_category_desc', 10 );
function woocommerce_category_desc() {
    if ( is_product_category() ){
	    global $wp_query;
	    $term_object = get_queried_object();
        ?>
        <div class="woocommerce-category-description">
            <div class="description"><?php echo $term_object->description; ?></div>
        </div>
	<?php }
    }


//code captcha
function disable_recaptcha_badge_page(){
    if ( is_front_page() || is_home() || is_shop() ) {
        wp_dequeue_script('woo-slg-public-script');
        wp_dequeue_script('woo-slg-unlink-script');
    }
    if ( ! is_user_logged_in() ) {
        wp_dequeue_style( 'dashicons' );
        wp_deregister_style( 'dashicons' );
        wp_deregister_style('wp_deregister_style');
        
    }
 }
 add_filter( 'jetpack_sharing_counts', '__return_false', 99 );
add_filter( 'jetpack_implode_frontend_css', '__return_false', 99 );
 add_action( 'wp_enqueue_scripts', 'disable_recaptcha_badge_page' );
 function remove_jetpack_styles(){
    wp_deregister_style('AtD_style'); // After the Deadline
    wp_deregister_style('jetpack-carousel'); // Carousel
    wp_deregister_style('grunion.css'); // Grunion contact form
    wp_deregister_style('the-neverending-homepage'); // Infinite Scroll
    wp_deregister_style('infinity-twentyten'); // Infinite Scroll - Twentyten Theme
    wp_deregister_style('infinity-twentyeleven'); // Infinite Scroll - Twentyeleven Theme
    wp_deregister_style('infinity-twentytwelve'); // Infinite Scroll - Twentytwelve Theme
    wp_deregister_style('noticons'); // Notes
    wp_deregister_style('post-by-email'); // Post by Email
    wp_deregister_style('publicize'); // Publicize
    wp_deregister_style('sharedaddy'); // Sharedaddy
    wp_deregister_style('sharing'); // Sharedaddy Sharing
    wp_deregister_style('stats_reports_css'); // Stats
    wp_deregister_style('jetpack-widgets'); // Widgets
    }
    add_action('wp_print_styles', 'remove_jetpack_styles');
    
    
/*
Footer Scripts
*/
add_action( 'wp_footer', 'product_footer_scripts',20 );
function product_footer_scripts(){
     if (isset($_GET['recensione'])) {  ?>
        <script>
        jQuery(document).ready(function(){
            jQuery("#tab-title-reviews > a").trigger( "click" );
        })
        </script>
        <?php } ?>
        <script>
            jQuery(document).ready(function(){
                jQuery('#close-nl').click(function(){
                    jQuery('#modal-newsletter').fadeOut();
                    setCookieModal('modalcookie','yes',7);
                });
                var x = getCookieModal('modalcookie');
                if (x) {
                    jQuery('#modal-newsletter').hide();
                }
            })
            
            function setCookieModal(name,value,days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days*24*60*60*1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + (value || "")  + expires + "; path=/";
            }
            function getCookieModal(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }
        </script>
<?php }

/*curl sendinblue */
/*
curl -H 'api-key:YOUR_API_V3_KEY' \
-X POST -d '{ \
# Define the campaign settings \
"name":"Campaign sent via the API", \
"subject":"My subject", \
"sender": { "name": "From name", "email":"bruciaessenze@gmail.com" }, \
"type": "classic", \
# Content that will be sent \
"htmlContent": "Congratulations! You successfully sent this example campaign via the Sendinblue API.", \
# Select the recipients\
"recipients": { "listIds": [2,7] }, \
# Schedule the sending in one hour\
"scheduledAt": "2018-01-01 00:00:01", \
}'
'https://api.sendinblue.com/v3/emailCampaigns'

*/

/*script sendinblue */

add_action('admin_footer', 'admin_order_script');

function admin_order_script() {
    $screen = get_current_screen();
    global $post;
    $postid = $post->ID;   
    if( get_post_type( $postid ) === 'shop_order' ) {
    $order_id = get_the_ID();
    $order = wc_get_order( $order_id );

    $order_data = $order->get_data(); // The Order data
	$email = $order_data['billing']['email'];
	$first_name = $order_data['billing']['first_name'];
	$last_name = $order_data['billing']['last_name'];
	//https://www.businessbloomer.com/woocommerce-easily-get-order-info-total-items-etc-from-order-object/
	//prodotti
	$items = $order->get_items();
	$count = count($items);
    $i = 0;
    echo '
    <script type="text/javascript">
(function() {
    window.sib = {
        equeue: [],
        client_key: "rzn2sqfz036360uyaip0pyxq"
    };
    /* OPTIONAL: email for identify request*/
    // window.sib.email_id = \'example@domain.com\';
    window.sendinblue = {};
    for (var j = [\'track\', \'identify\', \'trackLink\', \'page\'], i = 0; i < j.length; i++) {
    (function(k) {
        window.sendinblue[k] = function() {
            var arg = Array.prototype.slice.call(arguments);
            (window.sib[k] || function() {
                    var t = {};
                    t[k] = arg;
                    window.sib.equeue.push(t);
                })(arg[0], arg[1], arg[2]);
            };
        })(j[i]);
    }
    var n = document.createElement("script"),
        i = document.getElementsByTagName("script")[0];
    n.type = "text/javascript", n.id = "sendinblue-js", n.async = !0, n.src = "https://sibautomation.com/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sendinblue.page();
})();
</script>
    ';
    echo '
    <script>
	jQuery(document).ready(function() {
        
			var event_name = "ordine_completo";
			var properties = {
				"email_id": "'.$email.'",
				"NOME": "'.$first_name.'",
				"COGNOME": "'.$last_name.'",
			}
			var event_data = {
				"id": "'. $order_id .'",
				"data": {
					"items": [';
						foreach ($items as $item) {
							$product_name = $item->get_name();
							$product_id = $item->get_product_id();
                            $permalink = get_permalink($product_id);
							$thumbnail = get_the_post_thumbnail_url($product_id,'medium');
							echo "{";
							echo "\"name\": \"$product_name\",";
							echo "\"url\": \"$permalink\",";
							echo "\"image\": \"$thumbnail\"";
							if ($i == $count - 1) {
								echo "}]";
							} else {
								echo "},";
							}
								$i++;
							} 
					echo '	}
                    }
        if(jQuery("#original_post_status").val() =="wc-completed") {
			sendinblue.track(
				event_name, 
				properties, 
				event_data 
			)
        }
	});
</script>';
    }
}
//data structure
// overwrites structured data
add_filter( 'woocommerce_structured_data_product',  'set_structured_data', 99, 2 );
function set_structured_data( $markup, $product ) {    
    $id = $product->get_id();
    $product = wc_get_product($id);
    $markup['brand'] = $product->get_attribute('pa_brand');
    $markup['sku'] = $product->get_id();
    //$markup['description'] = wp_strip_all_tags( $product->get_short_description() ? $product->get_short_description() : $product->get_description() );
    return $markup;
}
?>