<?php

/**
 * Template Name: Ordine completo
 *
 * Description: Webhook from order complete
 *
 * @package WordPress
 * @subpackage Makali_Theme
 * @since Makali 1.0
 */
$makali_opt = get_option('makali_opt');

get_header();
$url = 'https://bruciaessenze.it';
//https://www.bruciaessenze.it/ordine-completo/?password=ajvs
if (isset($_GET['password']) && $_GET['password'] == 'ajvs') {
} else {
	header('Location: ' . $url, true, $permanent ? 301 : 302);
	exit();
}

/* curl */
<?php

$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => "https://api.sendinblue.com/v3/smtp/email",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"sender\":{\"name\":\"Bruciaessenze\",\"email\":\"no-reply@bruciaessenze.it\"},\"to\":[{\"email\":\"simone.puliti@gmail.com\",\"name\":\"Simone\"}],\"replyTo\":{\"email\":\"simone.puliti@gmail.com\"},\"headers\":{\"newKey\":\"\"},\"subject\":\"test\",\"htmlContent\":\"prova\"}",
  CURLOPT_HTTPHEADER => [
    "Accept: application/json",
    "Content-Type: application/json",
    "api-key: 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"
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

?>
<div class="main-container about-page">
	<div class="about-container">
		<?php while (have_posts()) : the_post(); ?>
			<?php // get_template_part('content', 'page'); 
			echo 'response:';
			if ($err) {
				echo "cURL Error #:" . $err;
			  } else {
				echo $response;
			  }
			
			?>
		<?php endwhile; ?>
	</div>
</div>

<script type="text/javascript">
/*
(function() {
    window.sib = {
        equeue: [],
        client_key: "rzn2sqfz036360uyaip0pyxq"
    };
    // window.sib.email_id = \'example@domain.com\';
    window.sendinblue = {};
    for (var j = ['track', 'identify', 'trackLink', 'page'], i = 0; i < j.length; i++) {
    (function(k) {
        window.sendinblue[k] = function() {
            var arg = Array.prototype.slice.call(arguments);
            (window.sib[k] || function() {
                    var t = {};
                    t[k] = arg;
                    window.sib.equeue.push(t);
                })(arg[0], arg[1], arg[2]);
            };
        })(j[i]);
    }
    var n = document.createElement("script"),
        i = document.getElementsByTagName("script")[0];
    n.type = "text/javascript", n.id = "sendinblue-js", n.async = !0, n.src = "https://sibautomation.com/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sendinblue.page();
})();/
</script>


<?php 
function complete_order_script($id,$n) {
    $order_id = $id;
    $order = wc_get_order( $order_id );
    $order_data = $order->get_data(); // The Order data
	$email = $order_data['billing']['email'];
	$first_name = $order_data['billing']['first_name'];
	$last_name = $order_data['billing']['last_name'];
	//https://www.businessbloomer.com/woocommerce-easily-get-order-info-total-items-etc-from-order-object/
	//prodotti
	$items = $order->get_items();

	$count = count($items);
	if($count == 0) {
		return;
	}
    $i = 0;
	
    echo '   
			var event_name'.$n.' = "ordine_completo";
			var properties'.$n.' = {
				"email_id": "'.$email.'",
				"NOME": "'.$first_name.'",
				"COGNOME": "'.$last_name.'",
			}
			var event_data'.$n.' = {
				"id": "'. $order_id .'",
				"data": {
					"items": [';
						foreach ($items as $item) {
								$product_name = $item->get_name();
								$product_id = $item->get_product_id();
								$permalink = get_permalink($product_id);
								$thumbnail = get_the_post_thumbnail_url($product_id,'medium');
								echo "{";
								echo "\"name\": \"$product_name\",";
								echo "\"url\": \"$permalink\",";
								echo "\"image\": \"$thumbnail\"";
								echo "},";
							} 
					echo '
						]	
					}
				}
			sendinblue.track(
				event_name'.$n.', 
				properties'.$n.', 
				event_data'.$n.' 
			);';
} ?>
<?php get_footer(); ?>